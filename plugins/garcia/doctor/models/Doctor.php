<?php namespace Garcia\Doctor\Models;

use Model;

/**
 * Doctor Model
 */
class Doctor extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'type'      => 'required',
        'name'      => 'required',
    ];

    public $attributeNames = [
        'type' => 'jabatan',
        'name' => 'nama',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'garcia_doctor_doctors';

    /**
     * @var string The database table used by the model.
     */
    public $jsonable = ['schedule'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'type' => [
            'Garcia\Doctor\Models\Type',
            'key'      => 'type_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'picture' => ['System\Models\File']
    ];
    public $attachMany    = [];

    public function beforeSave()
    {
        $this->slug = str_slug($this->name);
    }
}
