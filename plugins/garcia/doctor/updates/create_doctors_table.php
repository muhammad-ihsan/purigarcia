<?php namespace Garcia\Doctor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDoctorsTable extends Migration
{
    public function up()
    {
        Schema::create('garcia_doctor_doctors', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('type_id');
            $table->string('name');
            $table->text('schedule');
            $table->timestamps();
            $table->string('slug');
            $table->boolean('is_pinned')->nullable()->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('garcia_doctor_doctors');
    }
}
