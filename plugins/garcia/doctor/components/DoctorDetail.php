<?php namespace Garcia\Doctor\Components;

use Flash;
use Redirect;

use Cms\Classes\ComponentBase;

use Garcia\Doctor\Models\Doctor as DoctorModels;

class DoctorDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'DoctorDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $doctor = $this->getCurrent();
        if(!$doctor) {
            Flash::error('Terjadi kesalahan');
            return Redirect::to('404');
        }

        $this->page->title    = $doctor->name;
        $this->page['doctor'] = $doctor;
    }

    public function getCurrent()
    {
        return DoctorModels::whereSlug($this->property('parameter'))->first();
    }
}
