<?php namespace Garcia\Doctor\Components;

use Cms\Classes\ComponentBase;

use Garcia\Doctor\Models\Doctor as DoctorModels;
use Garcia\Doctor\Models\Type as TypeModels;

class Doctor extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Doctor Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return DoctorModels::orderBy('name', 'asc')->get();
    }

    public function getPinned()
    {
        return DoctorModels::orderBy('name')->whereIsPinned(1)->take(5)->get();
    }

    public function getType()
    {
        return TypeModels::orderBy('name')->get();
    }

    public function getByType($typeId)
    {
        return DoctorModels::whereTypeId($typeId)->get();
    }
}
