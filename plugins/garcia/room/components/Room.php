<?php namespace Garcia\Room\Components;

use Cms\Classes\ComponentBase;

use Garcia\Room\Models\Room as RoomModels;

class Room extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Room Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return RoomModels::orderBy('name', 'asc')->get();
    }
}
