<?php namespace Garcia\Room\Components;

use Cms\Classes\ComponentBase;

use Garcia\Room\Models\Room as RoomModels;

class RoomDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'RoomDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function getCurrent()
    {
        return RoomModels::whereSlug($this->property('parameter'))->first();
    }

    public function onRun()
    {
        $room = $this->getCurrent();
        if(!$room) {
            Flash::error('Terjadi kesalahan');
            return Redirect::to('404');
        }

        $this->page->title  = $room->name;
        $this->page['room'] = $room;
    }
}
