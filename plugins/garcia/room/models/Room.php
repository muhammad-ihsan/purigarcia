<?php namespace Garcia\Room\Models;

use Model;

/**
 * Room Model
 */
class Room extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'name'        => 'required',
        'content'     => 'required',
    ];

    public $attributeNames = [
        'name'        => 'nama',
        'content'     => 'konten',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'garcia_room_rooms';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'picture' => ['System\Models\File']
    ];
    public $attachMany    = [
        'galleries' => ['System\Models\File']
    ];

    public function beforeSave()
    {
        $this->slug = str_slug($this->name);
    }
}
