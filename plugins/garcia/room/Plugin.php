<?php namespace Garcia\Room;

use Backend;
use System\Classes\PluginBase;

/**
 * Room Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Room',
            'description' => 'No description provided yet...',
            'author'      => 'Garcia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Garcia\Room\Components\Room'       => 'Room',
            'Garcia\Room\Components\RoomDetail' => 'RoomDetail',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'garcia.room.some_permission' => [
                'tab' => 'Room',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'room' => [
                'label'       => 'Room',
                'url'         => Backend::url('garcia/room/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['garcia.room.*'],
                'order'       => 500,
            ],
        ];
    }
}
