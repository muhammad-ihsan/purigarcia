<?php namespace Garcia\Service;

use Backend;
use System\Classes\PluginBase;

/**
 * Service Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Service',
            'description' => 'No description provided yet...',
            'author'      => 'Garcia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Garcia\Service\Components\Service' => 'Service',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'garcia.service.some_permission' => [
                'tab' => 'Service',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'service' => [
                'label'       => 'Service',
                'url'         => Backend::url('garcia/service/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['garcia.service.*'],
                'order'       => 500,
            ],
        ];
    }
}
