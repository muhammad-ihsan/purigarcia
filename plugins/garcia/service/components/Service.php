<?php namespace Garcia\Service\Components;

use Cms\Classes\ComponentBase;

use Garcia\Service\Models\Service as ServiceModels;

class Service extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Service Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return ServiceModels::orderBy('name')->get();
    }
}
