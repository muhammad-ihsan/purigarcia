<?php namespace Garcia\Facility\Components;

use Cms\Classes\ComponentBase;

use Garcia\Facility\Models\Facility as FacilityModels;

class Facility extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Facility Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return FacilityModels::orderBy('created_at', 'desc')->get();
    }
}
