<?php namespace Garcia\Facility\Models;

use Model;

/**
 * Facility Model
 */
class Facility extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'name'    => 'required',
    ];

    public $attributeNames = [
        'name'    => 'nama',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'garcia_facility_facilities';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [
        'galleries' => ['System\Models\File']
    ];

    public function beforeSave()
    {
        $this->slug = str_slug($this->name);
    }
}
