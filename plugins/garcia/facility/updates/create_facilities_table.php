<?php namespace Garcia\Facility\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFacilitiesTable extends Migration
{
    public function up()
    {
        Schema::create('garcia_facility_facilities', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->string('slug');
        });
    }

    public function down()
    {
        Schema::dropIfExists('garcia_facility_facilities');
    }
}
