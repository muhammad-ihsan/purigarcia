<?php namespace Garcia\Core;

use Backend;
use System\Classes\PluginBase;

/**
 * Core Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Core',
            'description' => 'No description provided yet...',
            'author'      => 'Garcia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \Event::listen('cms.page.beforeDisplay', function($controller, $page, $url) {
            $controller->vars['settings'] = [
                'site'       => \Garcia\Core\Models\Site::instance(),
                'general'    => \Garcia\Core\Models\General::instance(),
                'appearance' => \Garcia\Core\Models\Appearance::instance(),
            ];
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Garcia\Core\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'garcia.core.some_permission' => [
                'tab' => 'Core',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'core' => [
                'label'       => 'Puri Garcia',
                'url'         => Backend::url('garcia/core/appointments'),
                'icon'        => 'icon-plus',
                'iconSvg'     => 'plugins/garcia/core/assets/images/icon-hospital.svg',
                'permissions' => ['garcia.core.*'],
                'order'       => 900,

                'sideMenu' => [
                    'appointments' => [
                        'label'       => 'Konsultasi',
                        'url'         => Backend::url('garcia/core/appointments'),
                        'icon'        => 'icon-list',
                        'permissions' => ['garcia.core.*'],
                        'order'       => 500,
                    ],
                    'doctors' => [
                        'label'       => 'Dokter',
                        'url'         => Backend::url('garcia/core/doctors'),
                        'icon'        => 'icon-user-md',
                        'permissions' => ['garcia.core.*'],
                        'order'       => 500,
                    ],
                    'rooms' => [
                        'label'       => 'Ruangan',
                        'url'         => Backend::url('garcia/core/rooms'),
                        'icon'        => 'icon-hospital-o',
                        'permissions' => ['garcia.core.*'],
                        'order'       => 500,
                    ],
                    'services' => [
                        'label'       => 'Layanan',
                        'url'         => Backend::url('garcia/core/services'),
                        'icon'        => 'icon-stethoscope',
                        'permissions' => ['garcia.core.*'],
                        'order'       => 500,
                    ],
                    'facilities' => [
                        'label'       => 'Fasilitas',
                        'url'         => Backend::url('garcia/core/facilities'),
                        'icon'        => 'icon-h-square',
                        'permissions' => ['garcia.core.*'],
                        'order'       => 500,
                    ],
                    'careers' => [
                        'label'       => 'Lowongan',
                        'url'         => Backend::url('garcia/core/careers'),
                        'icon'        => 'icon-briefcase',
                        'permissions' => ['garcia.core.*'],
                        'order'       => 500,
                    ],
                ]
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'site' => [
                'label'       => 'Site',
                'description' => 'Manage website content page.',
                'category'    => 'Site',
                'icon'        => 'icon-file',
                'class'       => 'Garcia\Core\Models\Site',
                'order'       => 100,
            ],
            'general' => [
                'label'       => 'General',
                'description' => 'Manage general site configuration.',
                'category'    => 'Site',
                'icon'        => 'icon-cog',
                'class'       => 'Garcia\Core\Models\General',
                'order'       => 101,
            ],
            'appearance' => [
                'label'       => 'Appearance',
                'description' => 'Manage appearance site configuration.',
                'category'    => 'Site',
                'icon'        => 'icon-image',
                'class'       => 'Garcia\Core\Models\Appearance',
                'order'       => 102,
            ],
        ];
    }
}
