<?php namespace Garcia\Core\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Doctors Back-end Controller
 */
class Doctors extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        $this->bodyClass = 'compact-container';
        BackendMenu::setContext('Garcia.Core', 'core', 'doctors');
    }
}
