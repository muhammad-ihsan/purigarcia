<?php namespace Garcia\Core\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Doctor Types Back-end Controller
 */
class DoctorTypes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Garcia.Core', 'core', 'doctortypes');
    }
}
