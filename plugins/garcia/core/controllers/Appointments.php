<?php namespace Garcia\Core\Controllers;

use Flash;
use Redirect;
use Renatio\DynamicPDF\Classes\PDF;

use BackendMenu;
use Backend\Classes\Controller;

use Garcia\Appointment\Models\Appointment as AppointmentModels;

/**
 * Appointments Back-end Controller
 */
class Appointments extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Garcia.Core', 'core', 'appointments');
    }

    public function onPreviewPdf($recordId = null)
    {
        $appointment  = AppointmentModels::whereId($recordId)->first();
        return Redirect::to('/schedule/pdf/'.$appointment->parameter);
    }
}
