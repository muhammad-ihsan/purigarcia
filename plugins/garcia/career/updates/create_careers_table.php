<?php namespace Garcia\Career\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCareersTable extends Migration
{
    public function up()
    {
        Schema::create('garcia_career_careers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('content');
            $table->date('closed_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('garcia_career_careers');
    }
}
