<?php namespace Garcia\Career\Models;

use Model;

/**
 * Career Model
 */
class Career extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'name'      => 'required',
        'closed_at' => 'required',
        'content'   => 'required',
    ];

    public $attributeNames = [
        'name'      => 'posisi',
        'closed_at' => 'tanggal ditutup',
        'content'   => 'konten',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'garcia_career_careers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
