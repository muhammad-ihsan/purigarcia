<?php namespace Garcia\Career\Components;

use Cms\Classes\ComponentBase;

use Garcia\Career\Models\Career as CareerModels;

class Career extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Career Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return CareerModels::orderBy('closed_at', 'desc')->get();
    }
}
