<?php namespace Garcia\Career;

use Backend;
use System\Classes\PluginBase;

/**
 * Career Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Career',
            'description' => 'No description provided yet...',
            'author'      => 'Garcia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Garcia\Career\Components\Career' => 'Career',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'garcia.career.some_permission' => [
                'tab' => 'Career',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'career' => [
                'label'       => 'Career',
                'url'         => Backend::url('garcia/career/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['garcia.career.*'],
                'order'       => 500,
            ],
        ];
    }
}
