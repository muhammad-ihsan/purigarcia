<?php namespace Garcia\Appointment;

use Backend;
use System\Classes\PluginBase;

/**
 * Appointment Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Appointment',
            'description' => 'No description provided yet...',
            'author'      => 'Garcia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Garcia\Appointment\Components\Appointment'    => 'Appointment',
            'Garcia\Appointment\Components\AppointmentPdf' => 'AppointmentPdf',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'garcia.appointment.some_permission' => [
                'tab' => 'Appointment',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'appointment' => [
                'label'       => 'Appointment',
                'url'         => Backend::url('garcia/appointment/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['garcia.appointment.*'],
                'order'       => 500,
            ],
        ];
    }

    /**
     * [registerMailTemplates description]
     * @return [type] [description]
     */
    // public function registerMailTemplates()
    // {
    //     return [
    //         'garcia.appointment::mail.information',
    //     ];
    // }
}
