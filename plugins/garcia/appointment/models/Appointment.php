<?php namespace Garcia\Appointment\Models;

use Hash;
use Model;

/**
 * Appointment Model
 */
class Appointment extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $rules = [
        'doctor_id'     => 'required',
        'schedule_date' => 'required|date',
        'scheduled_at'  => 'required',
        'type'          => 'required|in:baru,lama',
        'name'          => 'required',
        'email'         => 'email',
        'phone'         => 'required',
    ];

    public $attributeNames = [
        'doctor_id'      => 'dokter',
        'scheduled_date' => 'jadwal',
        'scheduled_at'   => 'waktu',
        'type'           => 'jenis pasien',
        'name'           => 'nama',
        'phone'          => 'no telefon',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'garcia_appointment_appointments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'doctor' => [
            'Garcia\Doctor\Models\Doctor',
            'key'      => 'doctor_id',
            'otherKey' => 'id',
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $this->parameter = md5(Hash::make(date('Y-m-d H:i:s')).rand(0, 999));
    }

    public function afterCreate()
    {
        // $vars = ['name' => 'Joe', 'user' => 'Mary'];
        // Mail::send('acme.blog::mail.message', $vars, function($message) {
        //     $message->to('admin@domain.tld', 'Admin Person');
        //     $message->subject('This is a reminder');
        // });
    }
}
