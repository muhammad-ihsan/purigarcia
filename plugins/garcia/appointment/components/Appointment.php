<?php namespace Garcia\Appointment\Components;

use Carbon\Carbon;
use Mail;
use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Garcia\Doctor\Models\Doctor as DoctorModels;
use Garcia\Doctor\Models\Type as DoctorTypeModels;
use Garcia\Appointment\Models\Appointment as AppointMentModels;

class Appointment extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Appointment Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getDoctor()
    {
        return DoctorModels::orderBy('name')->get();
    }

    public function getMedic()
    {
        return DoctorTypeModels::orderBy('name')->get();
    }

    public function onSelectMedic()
    {
        $doctors               = DoctorModels::whereTypeId(post('medic_id'))->orderBy('name')->get();
        $this->page['doctors'] = $doctors;
    }

    public function onSelectDoctor()
    {
        $doctors              = DoctorModels::whereId(post('doctor_id'))->first();
        $this->page['doctor'] = $doctors;
    }

    public function onSelectPatient()
    {
        $this->page['type'] = post('type');
        return;
    }

    public function onSelectSchedule()
    {
        $this->page['post'] = post('scheduled_at');
        return;
    }

    public function onSend()
        {
        $days  = [];
        $rules = [
            'doctor_id'     => 'required',
            'schedule_date' => 'required|date',
            'scheduled_at'  => 'required',
            'type'          => 'required|in:baru,lama',
            'name'          => 'required',
            'email'         => 'email',
            'phone'         => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'doctor_id'      => 'dokter',
            'scheduled_date' => 'jadwal',
            'scheduled_at'   => 'waktu',
            'type'           => 'jenis pasien',
            'name'           => 'nama',
            'phone'          => 'no telefon',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $schedule_date = Carbon::parse(post('schedule_date'))->format('l');
        $doctor        = DoctorModels::whereId(post('doctor_id'))->first();

        if(post('schedule_at_day') != $schedule_date) {
            Flash::error('Tanggal tidak sesuai dengan jadwal dokter');
            return;
        }

        $appointment                = new AppointMentModels;
        $appointment->doctor_id     = post('doctor_id');
        $appointment->schedule_date = post('schedule_date');
        $appointment->scheduled_at  = post('scheduled_at');
        $appointment->type          = post('type');
        $appointment->record        = post('record');
        $appointment->identity      = post('identity');
        $appointment->name          = post('name');
        $appointment->email         = post('email');
        $appointment->phone         = post('phone');
        $appointment->address       = post('address');
        $appointment->rt            = post('rt');
        $appointment->province      = post('province');
        $appointment->regency       = post('regency');
        $appointment->district      = post('district');
        $appointment->village       = post('village');
        $appointment->postal        = post('postal');
        $appointment->born_place    = post('born_place');
        $appointment->dob           = post('dob');
        $appointment->blood_type    = post('blood_type');
        $appointment->religion      = post('religion');
        $appointment->job           = post('job');
        $appointment->education     = post('education');
        $appointment->marital       = post('marital');
        $appointment->allergy       = post('allergy');
        $appointment->pic_identity  = post('pic_identity');
        $appointment->pic_name      = post('pic_name');
        $appointment->pic_phone     = post('pic_phone');
        $appointment->pic_relation  = post('pic_relation');
        $appointment->save();

        $vars = ['appointment' => $appointment];

        Mail::send('garcia.appointment::mail.information', $vars, function($message) use ($appointment) {
            $message->subject('Pemesanan baru untuk '.$appointment->doctor->name);
            if($appointment->email) {
                $message->from($appointment->email, $appointment->name);
            }
            else {
                $message->from('kontak@rspurigarcia.com', $appointment->name);
            }
            $message->cc('kontak@rspurigarcia.com', 'RSIA PURI GARCIA');
            $message->to(env('APPOINTMENT_MAIL'));
        });

        if(post('email')) {
            Mail::send('garcia.appointment::mail.information-customer', $vars, function($message) use ($appointment) {
                $message->subject('Pemesanan kamu telah kami terima');
                $message->from(env('APPOINTMENT_MAIL'), 'RSIA PURI GARCIA');
                $message->to($appointment->email);
            });
        }

        Flash::success('Berhasil membuat janji');
        // return Redirect::refresh();
    }
}
