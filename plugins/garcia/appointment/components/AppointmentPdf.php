<?php namespace Garcia\Appointment\Components;

use Flash;
use Redirect;
use Renatio\DynamicPDF\Classes\PDF;

use Cms\Classes\ComponentBase;

use Garcia\Appointment\Models\Appointment as AppointmentModels;

class AppointmentPdf extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AppointmentPdf Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $appointment = $this->getCurrent();
        if(!$appointment) {
            Flash::error('Terjadi kesalahan');
            return Redirect::to('404');
        }

        $templateCode = 'rsgarcia::appointment';
        $data         = [
            'appointment' => $appointment,
            'general' => \Garcia\Core\Models\General::instance()
        ];
        return PDF::loadTemplate($templateCode, $data)->stream('download.pdf');
    }

    public function getCurrent()
    {
        return AppointmentModels::whereParameter($this->property('parameter'))->first();
    }
}
