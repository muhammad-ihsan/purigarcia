<?php namespace Garcia\Appointment\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAppointmentsTable extends Migration
{
    public function up()
    {
        Schema::create('garcia_appointment_appointments', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('doctor_id');
            $table->string('scheduled_at');
            $table->enum('type', ['baru', 'lama']);
            $table->string('record')->nullable();
            $table->string('identity')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->string('rt')->nullable();
            $table->string('province')->nullable();
            $table->string('regency')->nullable();
            $table->string('district')->nullable();
            $table->string('village')->nullable();
            $table->string('postal')->nullable();
            $table->string('born_place')->nullable();
            $table->date('dob')->nullable();
            $table->string('blood_type')->nullable();
            $table->string('religion')->nullable();
            $table->string('job')->nullable();
            $table->string('education')->nullable();
            $table->string('marital')->nullable();
            $table->string('allergy')->nullable();
            $table->string('pic_identity')->nullable();
            $table->string('pic_name')->nullable();
            $table->string('pic_phone')->nullable();
            $table->string('pic_relation')->nullable();
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('garcia_appointment_appointments');
    }
}
