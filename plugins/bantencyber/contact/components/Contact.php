<?php namespace Bantencyber\Contact\Components;

use Redirect;
use Flash;

use Cms\Classes\ComponentBase;

use Bantencyber\Contact\Models\Contact as ContactModels;

class Contact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Contact Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onContact()
    {
        $contact          = new ContactModels;
        $contact->name    = post('name');
        $contact->email   = post('email');
        $contact->phone   = post('phone');
        $contact->subject = post('subject');
        $contact->message = post('message');
        $contact->save();
        Flash::success('Berhasil mengirim pesan');
        return Redirect::refresh();
    }
}
